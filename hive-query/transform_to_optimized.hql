create external table nielsendata.streamdata_optimized_temp(
	upid String,
	visitor String,
  	vid String,
	muid String,
  	start_at timestamp,
  	end_at timestamp,
  	duration_from int,
  	duration_to int,
	ua String,
	start_at_date_str String,
	start_at_time_str String,
	timespent_str String,
	country_name String, 
	city_name String, 
	customer_stage String, 
	video_stage String, 
	network_type String, 
	device_name String,
	device_type String
)
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata_optimized_temp";


/*from streamdata_orc tp streamdata_optimized_temp [transform old format to streamdata_optimized]**/
insert into streamdata_optimized_temp
select 
	s.upid as upid,
	s.visitor as visitor,
	s.videoid as vid,
	s.muid as muid,
	cast(from_unixtime(unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC'),'yyyy-MM-dd HH:mm:ss') as timestamp) as start_at,
	cast(from_unixtime((unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC') + cast(s.timespent as bigint)),'yyyy-MM-dd HH:mm:ss') as timestamp) as end_at,
	0 as duration_from,
	cast(s.timespent as int) as duration_to,
	s.ua as ua,
	s.hkdate as start_at_date_str,
	s.hktime as start_at_time_str,
	s.timespent as timespent_str,
	s.countryname as country_name,
	s.cityname as city_name,
	null as customer_stage, 
	null as video_stage, 
	null as network_type, 
	null as device_name,
	null as device_type
from
	streamdata_orc s
where 
	s.actiontypename = 'START'
	and s.ua between 22 and 24
	and s.hkdate like '2016-03-%'
;




——————————————————————————————————————————————————————————————————————————————————————————————————————

insert into streamdata_optimized
SELECT 
	upid,
	vid,
	muid,
	start_at,
	end_at,
	duration_from,
	duration_to,
	ua
FROM 
	streamdata_optimized_temp
where 
	day(end_at) - day(start_at) = 0;


SELECT 
	videoid as channel,
	recordcount as record_count,
	totalminutes as total_minutes_viewed,
	(totalminutes/recordcount) as average_minutes_viewed
FROM 
	octmonthlytemp;





SELECT
videoid,
count(*) as Monthly count
SUM(cast(timespent as int) / 60) as 'total time spent in minutes'
FROM 
streamdata_orc
where
hkdate like '2016-10-%'
and videoid IN ('live86', 'live87', 'live88', 'live89', 'live90', 'live91', 'live92', 'live93', 'live94', 'live95', 'live96', 'live97')
group by 
videoid
order by 
videoid
desc;



insert into octweeklytemp
SELECT
	videoid,
	weekofyear(hkdate) as week_number,
	count(*) as weekly_count,
	SUM(cast(timespent as int) / 60) as total_minutes_spent
FROM 
 	streamdata_orc
where
	hkdate like '2016-10-%'
	and videoid IN ('live86', 'live87', 'live88', 'live89', 'live90', 'live91', 'live92', 'live93', 'live94', 'live95', 'live96', 'live97')
group by 
	weekofyear(hkdate), videoid
order by 
	videoid
desc;


