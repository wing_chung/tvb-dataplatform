/* create external table nielsendata.streamdata */

add jar wasbs://libs@tvbhkanalyticsstorage1.blob.core.windows.net/csv-serde-1.1.2-0.11.0-all.jar;

create external table nielsendata.streamdata(TIME STRING, HKDATE STRING, HKTIME STRING, CONTENTGROUP STRING, ACTIONTYPENAME STRING, TIMESPENT STRING, VISITOR STRING, REFERRINGURL STRING, DOCUMENTTITLE STRING, LANGUAGECODE STRING, BROWSERNAME STRING, BROWSERVERSIONNAME STRING, OPERATINGSYSTEMNAME STRING, OPERATINGSYSTEMVERSIONNAME STRING, COUNTRYNAME STRING, CITYNAME STRING, SCREENRESOLUTION STRING, MUID STRING, UA STRING, DEVICENAME STRING, DEVICETYPE STRING, PAGEPATH STRING, VIDEOID STRING, UPID STRING, APPNAME STRING, APPVERSIONNAME STRING, HASSTORE STRING, ATTRIBUTES STRING, CUSTOMER_STAGE STRING, VIDEO_STAGE STRING, QUALITY_LABEL STRING, NETWORK_TYPE STRING, RESOLUTION STRING) 
ROW FORMAT serde 'com.bizo.hive.serde.csv.CSVSerde'
 with serdeproperties (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "escapeChar"    = "\\"
 ) 
STORED AS TEXTFILE 
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata" 
TBLPROPERTIES("skip.header.line.count"="1")
;


/* create external table nielsendata.streamdata2 */

add jar wasbs://libs@tvbhkanalyticsstorage1.blob.core.windows.net/csv-serde-1.1.2-0.11.0-all.jar;

create external table nielsendata.streamdata2(TIME STRING, HKDATE STRING, HKTIME STRING, CONTENTGROUP STRING, ACTIONTYPENAME STRING, TIMESPENT STRING, VISITOR STRING, REFERRINGURL STRING, DOCUMENTTITLE STRING, LANGUAGECODE STRING, BROWSERNAME STRING, BROWSERVERSIONNAME STRING, OPERATINGSYSTEMNAME STRING, OPERATINGSYSTEMVERSIONNAME STRING, COUNTRYNAME STRING, CITYNAME STRING, SCREENRESOLUTION STRING, MUID STRING, UA STRING, DEVICENAME STRING, DEVICETYPE STRING, PAGEPATH STRING, VIDEOID STRING, UPID STRING, APPNAME STRING, APPVERSIONNAME STRING, HASSTORE STRING, ATTRIBUTES STRING, CUSTOMER_STAGE STRING, VIDEO_STAGE STRING, QUALITY_LABEL STRING, NETWORK_TYPE STRING, RESOLUTION STRING) 
ROW FORMAT serde 'com.bizo.hive.serde.csv.CSVSerde'
 with serdeproperties (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "escapeChar"    = "\\"
 ) 
STORED AS TEXTFILE 
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata2" 
TBLPROPERTIES("skip.header.line.count"="1");

——————————————————————————————————————————————————————————————————————————————————————————————————————
//create external table nielsendata.webdata

add jar wasbs://libs@tvbhkanalyticsstorage1.blob.core.windows.net/csv-serde-1.1.2-0.11.0-all.jar;

create external table nielsendata.webdata(HKDATE STRING, HKTIME STRING,TIME STRING,CONTENTGROUP STRING,VISITOR STRING,REFERRINGURL STRING,LANGUAGECODE STRING,BROWSERNAME STRING,BROWSERVERSIONNAME STRING,OPERATINGSYSTEMNAME STRING,OPERATINGSYSTEMVERSIONNAME STRING,COUNTRYNAME STRING,CITYNAME STRING,ACTIONTYPENAME STRING,PAGEPATH STRING,PAGETITLE STRING,SCREENRESOLUTION STRING,COLORDEPTH STRING,CHARSET STRING,FLASHVERSION STRING,MUID STRING,UA STRING,DEVICENAME STRING,DEVICETYPE STRING,APPNAME STRING,APPVERSIONNAME STRING,HASSTORE STRING) 
ROW FORMAT serde 'com.bizo.hive.serde.csv.CSVSerde'
with serdeproperties (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "escapeChar"    = "\\"
 ) 
STORED AS TEXTFILE 
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/webdata" 
TBLPROPERTIES("skip.header.line.count"="1")
;
——————————————————————————————————————————————————————————————————————————————————————————————————————
//create external table nielsendata.webdata2

add jar wasbs://libs@tvbhkanalyticsstorage1.blob.core.windows.net/csv-serde-1.1.2-0.11.0-all.jar;

create external table nielsendata.webdata2(HKDATE STRING, HKTIME STRING,TIME STRING,CONTENTGROUP STRING,VISITOR STRING,REFERRINGURL STRING,LANGUAGECODE STRING,BROWSERNAME STRING,BROWSERVERSIONNAME STRING,OPERATINGSYSTEMNAME STRING,OPERATINGSYSTEMVERSIONNAME STRING,COUNTRYNAME STRING,CITYNAME STRING,ACTIONTYPENAME STRING,PAGEPATH STRING,PAGETITLE STRING,SCREENRESOLUTION STRING,COLORDEPTH STRING,CHARSET STRING,FLASHVERSION STRING,MUID STRING,UA STRING,DEVICENAME STRING,DEVICETYPE STRING,APPNAME STRING,APPVERSIONNAME STRING,HASSTORE STRING, ATTRIBUTES STRING, CUSTOMER_STAGE STRING, VIDEO_STAGE STRING, QUALITY_LABEL STRING, NETWORK_TYPE STRING, RESOLUTION STRING) 
ROW FORMAT serde 'com.bizo.hive.serde.csv.CSVSerde'
with serdeproperties (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "escapeChar"    = "\\"
 ) 
STORED AS TEXTFILE 
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/webdata2" 
TBLPROPERTIES("skip.header.line.count"="1");
——————————————————————————————————————————————————————————————————————————————————————————————————————
//create external table nielsendata.streamdata_orc
create external table nielsendata.streamdata_orc(TIME STRING, HKDATE STRING, HKTIME STRING, CONTENTGROUP STRING, ACTIONTYPENAME STRING, TIMESPENT STRING, VISITOR STRING, REFERRINGURL STRING, DOCUMENTTITLE STRING, LANGUAGECODE STRING, BROWSERNAME STRING, BROWSERVERSIONNAME STRING, OPERATINGSYSTEMNAME STRING, OPERATINGSYSTEMVERSIONNAME STRING, COUNTRYNAME STRING, CITYNAME STRING, SCREENRESOLUTION STRING, MUID STRING, UA STRING, DEVICENAME STRING, DEVICETYPE STRING, PAGEPATH STRING, VIDEOID STRING, UPID STRING, APPNAME STRING, APPVERSIONNAME STRING, HASSTORE STRING, ATTRIBUTES STRING, CUSTOMER_STAGE STRING, VIDEO_STAGE STRING, QUALITY_LABEL STRING, NETWORK_TYPE STRING, RESOLUTION STRING
) 
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata_orc";
——————————————————————————————————————————————————————————————————————————————————————————————————————
add jar wasbs://libs@tvbhkanalyticsstorage1.blob.core.windows.net/csv-serde-1.1.2-0.11.0-all.jar;
INSERT INTO TABLE nielsendata.streamdata_orc SELECT * FROM nielsendata.streamdata;

——————————————————————————————————————————————————————————————————————————————————————————————————————
//create external table nielsendata.webdata_orc
create external table nielsendata.webdata_orc(HKDATE STRING, HKTIME STRING,TIME STRING,CONTENTGROUP STRING,VISITOR STRING,REFERRINGURL STRING,LANGUAGECODE STRING,BROWSERNAME STRING,BROWSERVERSIONNAME STRING,OPERATINGSYSTEMNAME STRING,OPERATINGSYSTEMVERSIONNAME STRING,COUNTRYNAME STRING,CITYNAME STRING,ACTIONTYPENAME STRING,PAGEPATH STRING,PAGETITLE STRING,SCREENRESOLUTION STRING,COLORDEPTH STRING,CHARSET STRING,FLASHVERSION STRING,MUID STRING,UA STRING,DEVICENAME STRING,DEVICETYPE STRING,APPNAME STRING,APPVERSIONNAME STRING,HASSTORE STRING
) 
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/webdata_orc";

——————————————————————————————————————————————————————————————————————————————————————————————————————
add jar wasbs://libs@tvbhkanalyticsstorage1.blob.core.windows.net/csv-serde-1.1.2-0.11.0-all.jar;
INSERT INTO TABLE nielsendata.webdata_orc SELECT * FROM nielsendata.webdata;
——————————————————————————————————————————————————————————————————————————————————————————————————————
//create external table nielsendata.streamdata2_orc
create external table nielsendata.streamdata2_orc(TIME STRING, HKDATE STRING, HKTIME STRING, CONTENTGROUP STRING, ACTIONTYPENAME STRING, TIMESPENT STRING, VISITOR STRING, REFERRINGURL STRING, DOCUMENTTITLE STRING, LANGUAGECODE STRING, BROWSERNAME STRING, BROWSERVERSIONNAME STRING, OPERATINGSYSTEMNAME STRING, OPERATINGSYSTEMVERSIONNAME STRING, COUNTRYNAME STRING, CITYNAME STRING, SCREENRESOLUTION STRING, MUID STRING, UA STRING, DEVICENAME STRING, DEVICETYPE STRING, PAGEPATH STRING, VIDEOID STRING, UPID STRING, APPNAME STRING, APPVERSIONNAME STRING, HASSTORE STRING, ATTRIBUTES STRING, CUSTOMER_STAGE STRING, VIDEO_STAGE STRING, QUALITY_LABEL STRING, NETWORK_TYPE STRING, RESOLUTION STRING) 
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata2_orc";
——————————————————————————————————————————————————————————————————————————————————————————————————————
//create external table nielsendata.webdata2_orc
create external table nielsendata.webdata2_orc(HKDATE STRING, HKTIME STRING,TIME STRING,CONTENTGROUP STRING,VISITOR STRING,REFERRINGURL STRING,LANGUAGECODE STRING,BROWSERNAME STRING,BROWSERVERSIONNAME STRING,OPERATINGSYSTEMNAME STRING,OPERATINGSYSTEMVERSIONNAME STRING,COUNTRYNAME STRING,CITYNAME STRING,ACTIONTYPENAME STRING,PAGEPATH STRING,PAGETITLE STRING,SCREENRESOLUTION STRING,COLORDEPTH STRING,CHARSET STRING,FLASHVERSION STRING,MUID STRING,UA STRING,DEVICENAME STRING,DEVICETYPE STRING,APPNAME STRING,APPVERSIONNAME STRING,HASSTORE STRING, ATTRIBUTES STRING, CUSTOMER_STAGE STRING, VIDEO_STAGE STRING, QUALITY_LABEL STRING, NETWORK_TYPE STRING, RESOLUTION STRING) 
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/webdata2_orc";


——————————————————————————————————————————————————————————————————————————————————————————————————————
//copy streamdata from wing-chung.inline.tvb.com to azure blob storage
blobxfer tvbhkanalyticsstorage1 nielsendata . --upload --include '*streamdata*.csv' --collate streamdata2 --autovhd --storageaccountkey hXjnSvgDzjuFP8HaUz7R2kJkyhxuprFn9E3kPNLl6nHejInyqI4rHwk1XifspnOufYC6G7sY4xMn1qy9ySYmoA==
——————————————————————————————————————————————————————————————————————————————————————————————————————
//copy webdata from wing-chung.inline.tvb.com to azure blob storage
blobxfer tvbhkanalyticsstorage1 nielsendata . --upload --include '*webdata*.csv' --collate webdata --autovhd --storageaccountkey hXjnSvgDzjuFP8HaUz7R2kJkyhxuprFn9E3kPNLl6nHejInyqI4rHwk1XifspnOufYC6G7sY4xMn1qy9ySYmoA==



——————————————————————————————————————————————————————————————————————————————————————————————————————
create external table nielsendata.streamdata_optimized_temp(
	visitor String,
	vidoid String,
	upid String,
	muid String,
  	start_at timestamp,
  	end_at timestamp,
  	duration_from int,
  	duration_to int,
	ua String,
	start_at_date_str String,
	start_at_time_str String,
	timespent_str String,
	country_name String, 
	city_name String, 
	customer_stage String, 
	video_stage String, 
	network_type String, 
	device_name String,
	device_type String
)
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata_optimized_temp";


create external table nielsendata.streamdata_optimized(
	visitor String,
	videoid String,
  	upid String,
	muid String,
  	start_at timestamp,
  	end_at timestamp,
  	duration_from int,
  	duration_to int,
	ua String,
	country_name String, 
	city_name String, 
	customer_stage String, 
	video_stage String, 
	network_type String 
)
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata_optimized";
——————————————————————————————————————————————————————————————————————————————————————————————————————
insert into streamdata_optimized_temp
select 
	s.upid as upid,
	s.videoid as vid,
	s.muid as muid,
	cast(from_unixtime(unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC'),'yyyy-MM-dd HH:mm:ss') as timestamp) as start_at,
	cast(from_unixtime((unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC') + cast(s.timespent as bigint)),'yyyy-MM-dd HH:mm:ss') as timestamp) as end_at,
	0 as duration_from,
	cast(s.timespent as int) as duration_to,
	s.ua as ua,
	s.hkdate as start_at_date_str,
	s.hktime as start_at_time_str,
	s.timespent as timespent_str
from
	streamdata_orc s
where 
	s.actiontypename = 'START'
	and s.ua between 22 and 24
	and s.hkdate like '2016-03-%'
;


create external table nielsendata.streamdata_optimized_temp(
	upid String,
  	vid String,
	muid String,
  	start_at timestamp,
  	end_at timestamp,
  	duration_from int,
  	duration_to int,
	ua String,
	start_at_date_str String,
	start_at_time_str String,
	timespent_str String
	
)
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata_optimized_temp";

——————————————————————————————————————————————————————————————————————————————————————————————————————

insert into streamdata_optimized
SELECT 
	upid,
	vid,
	muid,
	start_at,
	end_at,
	duration_from,
	duration_to,
	ua
FROM 
	streamdata_optimized_temp
where 
	day(end_at) - day(start_at) = 0;


SELECT 
	videoid as channel,
	recordcount as record_count,
	totalminutes as total_minutes_viewed,
	(totalminutes/recordcount) as average_minutes_viewed
FROM 
	octmonthlytemp;





SELECT
videoid,
count(*) as Monthly count
SUM(cast(timespent as int) / 60) as 'total time spent in minutes'
FROM 
streamdata_orc
where
hkdate like '2016-10-%'
and videoid IN ('live86', 'live87', 'live88', 'live89', 'live90', 'live91', 'live92', 'live93', 'live94', 'live95', 'live96', 'live97')
group by 
videoid
order by 
videoid
desc;



insert into octweeklytemp
SELECT
	videoid,
	weekofyear(hkdate) as week_number,
	count(*) as weekly_count,
	SUM(cast(timespent as int) / 60) as total_minutes_spent
FROM 
 	streamdata_orc
where
	hkdate like '2016-10-%'
	and videoid IN ('live86', 'live87', 'live88', 'live89', 'live90', 'live91', 'live92', 'live93', 'live94', 'live95', 'live96', 'live97')
group by 
	weekofyear(hkdate), videoid
order by 
	videoid
desc;

—————————
//transform from streamdata_orc(nielsen_new_format) to streamdata_optimized 
//it only applies one day data
//it only cover my tv super [ua = 22-24]
insert into streamdata_optimized
select 
	s.visitor as visitor, 
	s.videoid as vid, 
	s.upid as upid,
	s.muid as muid,
	min(cast(from_unixtime(unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC'),'yyyy-MM-dd HH:mm:ss') as timestamp)) as start_at,
	max(cast(from_unixtime(unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC'),'yyyy-MM-dd HH:mm:ss') as timestamp)) as end_at,
	min(cast(s.timespent as int)) as duration_from,
	max(cast(s.timespent as int)) as duration_to,
	s.ua as ua,
	collect_list(s.countryname)[0],
	collect_list(s.cityname)[0], 
	collect_list(s.customer_stage)[0],
	collect_list(s.video_stage)[0], 
	collect_list(s.network_type)[0] 
from 
	streamdata_orc s where hkdate = '2016-11-22' and ( s.ua = '22' or s.ua = '23' or s.ua = '24' ) group by visitor, videoid, upid, muid, ua;

—————————

//[OLD FORMAT]transform from streamdata_orc(nielsen_old_format) to streamdata_optimized 
insert into streamdata_optimized
select 
	s.visitor as visitor,
	s.videoid as vid, 
	s.upid as upid,
	s.muid as muid,
	cast(from_unixtime(unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC'),'yyyy-MM-dd HH:mm:ss') as timestamp) as start_at,
	cast(from_unixtime((unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC') + cast(s.timespent as bigint)),'yyyy-MM-dd HH:mm:ss') as timestamp) as end_at,
	0 as duration_from,
	cast(s.timespent as int) as duration_to,
	s.ua as ua,
	s.countryname,
	s.cityname, 
	s.customer_stage,
	s.video_stage, 
	s.network_type
from
	streamdata_orc s
where 
	s.actiontypename = 'START'
	and s.ua between 22 and 24
	and s.hkdate like '2016-03-%'
;


--------------

create external table nielsendata.streamdata_orc_partition
(TIME STRING, HKTIME STRING, CONTENTGROUP STRING, ACTIONTYPENAME STRING, TIMESPENT STRING, VISITOR STRING, REFERRINGURL STRING, DOCUMENTTITLE STRING, LANGUAGECODE STRING, BROWSERNAME STRING, BROWSERVERSIONNAME STRING, OPERATINGSYSTEMNAME STRING, OPERATINGSYSTEMVERSIONNAME STRING, COUNTRYNAME STRING, CITYNAME STRING, SCREENRESOLUTION STRING, MUID STRING, UA STRING, DEVICENAME STRING, DEVICETYPE STRING, PAGEPATH STRING, VIDEOID STRING, UPID STRING, APPNAME STRING, APPVERSIONNAME STRING, HASSTORE STRING, ATTRIBUTES STRING, CUSTOMER_STAGE STRING, VIDEO_STAGE STRING, QUALITY_LABEL STRING, NETWORK_TYPE STRING, RESOLUTION STRING)
PARTITIONED BY (HKDATE STRING)
STORED AS ORC
LOCATION "wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata_orc_partition";

insert into streamdata_orc_partition
PARTITION(hkdate, customer_stage, is_live )
select TIME, HKTIME, CONTENTGROUP, ACTIONTYPENAME, TIMESPENT, VISITOR, REFERRINGURL, DOCUMENTTITLE, LANGUAGECODE, BROWSERNAME, BROWSERVERSIONNAME, OPERATINGSYSTEMNAME, OPERATINGSYSTEMVERSIONNAME, COUNTRYNAME, CITYNAME, SCREENRESOLUTION, MUID, UA, DEVICENAME, DEVICETYPE, PAGEPATH, VIDEOID, UPID, APPNAME, APPVERSIONNAME, HASSTORE, ATTRIBUTES, VIDEO_STAGE, QUALITY_LABEL, NETWORK_TYPE, RESOLUTION, 
HKDATE, if(CUSTOMER_STAGE = 'free' or CUSTOMER_STAGE = 'paid', CUSTOMER_STAGE, 'others') as CUSTOMER_STAGE, if(instr(videoid, 'live') > 0, 'TRUE', 'FALSE' ) as IS_LIVE
from streamdata_orc
where hkdate like '2016-11-%';

insert into test
PARTITION(hkdate)
select TIME, HKTIME, CONTENTGROUP, ACTIONTYPENAME, TIMESPENT, VISITOR, REFERRINGURL, DOCUMENTTITLE, LANGUAGECODE, BROWSERNAME, BROWSERVERSIONNAME, OPERATINGSYSTEMNAME, OPERATINGSYSTEMVERSIONNAME, COUNTRYNAME, CITYNAME, SCREENRESOLUTION, MUID, UA, DEVICENAME, DEVICETYPE, PAGEPATH, VIDEOID, UPID, APPNAME, APPVERSIONNAME, HASSTORE, ATTRIBUTES, CUSTOMER_STAGE, VIDEO_STAGE, QUALITY_LABEL, NETWORK_TYPE, RESOLUTION, HKDATE
from test
where hkdate='2016-12-11';














