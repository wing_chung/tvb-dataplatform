/*dbo.account*/
SELECT boss_id, md5(mobile_number), customer_type, sales_channel, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at, status, id as account_id, title FROM boss_prod_20161013.accounts;

/*dbo.artiste*/
SELECT id as artiste_id, age, gender, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.artistes; 

/*dbo.artise_info*/
SELECT artiste_id, language, name, othername as other_name FROM mcm.artiste_infos;

/* dbo.attribute */
SELECT attribute_id, title_tc, title_sc, title_en, status, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_attribute;

/* dbo.attribute_list */
SELECT top_attribute_item_id as attribute_id,  attribute_list as child_attribute, title_tc, title_sc, title_en, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_attribute_list;


/* dbo.attribute_programme_mapping */
SELECT programme_id, attribute_id FROM sfm_mytvsuper.he_programme_attribute_relation;

/* dbo.bonus */
SELECT
	distinct
	r.id as bonus_id,
    r.owner_id as programme_id,
    if(r.created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', r.created_at),
    r.resource_container_type as bonsu_type
FROM 
	mcm.resource_containers r
inner join
	mcm.resource_container_items ri on ri.resource_container_id = r.id
where
    ri.resource_container_item_type = 'App\\Models\\Video'
    and r.owner_type = 'App\\Models\\Programme'
;

/* dbo.box_set */
SELECT id as box_set_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.box_sets;

/* dbo.campaign */
SELECT id as campaign_id, code as campaign_code, description, campaign_type, period_type, effective_from, effective_to, service_mean, channel_info, statement_desc_eng, statement_desc_chi, status, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_prod_20161013.campaigns;

/* dbo.campaign_product_mapping */
SELECT product_id, campaign_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_prod_20161013.campaign_details;

/* dbo.cast */
SELECT id as cast_id, box_set_id, gender, age, cast_type, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.casts;

/* dbo.cast_artiste_mapping */
SELECT cast_id, artiste_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.artiste_cast;

/* dbo.cast_info */
SELECT cast_id, language, name, othername as other_name, occupation, Null as personality, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.cast_infos;

/* dbo.channel */
SELECT channel_no, network_code, start_time, end_time, name_tc, name_en, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_channel_info;

/* dbo.crews  */
SELECT id as crew_id, age, gender, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.crews;

/* dbo.crews_box_set_mapping */
SELECT box_set_id, crew_id, crew_type, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.box_set_crew;


/* dbo.crew_infos  */
SELECT crew_id, language, name, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.crew_infos;


/* dbo.episode */
SELECT id as episode_id, box_set_id, episode_no, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.episodes;

/* dbo.episode_cast_mapping */
SELECT episode_id, cast_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.cast_episode;

/* dbo.episode_info */
SELECT episode_id, language, title, Null as synopsis, Null as synopsis_exad, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.episode_infos;


/* dbo.library */
SELECT lib_id as library_id, title_tc, title_sc, title_en, status, type, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_lib;

/* dbo.library_attribute_mapping */
SELECT lib_id as library_id, attribute_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_lib_top_attribute_item;

/* dbo.library_programme_mapping */
SELECT programme_id, lib_id as library_id FROM sfm_mytvsuper.he_lib_programme_relation;

/* dbo.product */
SELECT id as product_id, code, description, period_type, period, product_type, effective_from, effective_to, statement_desc_eng, statement_desc_chi, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_prod_20161013.products;

/* dbo.programme */
SELECT programme_id, programme_name as programme_name_tc, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at from sfm_mytvsuper.he_programmes;

/* dbo.programme_box_set_mapping */
SELECT box_set_id, programme_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_box_sets;

/* dbo.subscription */
SELECT account_id, id as subscription_id, campaign_id, contract_start, contract_end, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_prod_20161013.subscriptions;


/* dbo.subscription_product_mapping */
SELECT id as subscription_id, product_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_prod_20161013.subscribed_products;

/* dbo.ua */
SELECT ua, product_name, type, platform_type FROM nielsen_2016.maps_ua;


/* dbo.video */
/* find video with bonus_id  */
select 
    ri.resource_container_item_id as video_id,
    r.id as bonus_id,
	0 as episode_id,
    0 as channel_no,
	v.duration as duration,
	if(v.created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', v.created_at) as created_at
from  
	mcm.resource_containers r
inner join 
	mcm.resource_container_items ri
inner join
	mcm.videos v
where 
	r.owner_type = 'App\\Models\\Programme' and
    r.id = ri.resource_container_id and
    /**r.owner_id = 254061 and**/
    /**v.id = 254060 and**/
    ri.resource_container_item_id = v.id
;

/* find video with episode_id  */
select 
    ri.resource_container_item_id as video_id,
    0 as bonus_id,
	r.owner_id as episode_id,
    0 as channel_no,
	v.duration as duration,
	if(v.created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', v.created_at) as created_at
from  
	mcm.resource_containers r
inner join 
	mcm.resource_container_items ri
inner join
	mcm.videos v
where 
	r.owner_type = 'App\\Models\\Episode' and
    r.id = ri.resource_container_id and
    /**r.owner_id = 254061 and**/
    /**v.id = 254060 and**/
    ri.resource_container_item_id = v.id
;



- - - - - only for checking start — - - - - 
SELECT distinct owner_type FROM mcm.resource_containers;
Select distinct resource_container_item_type from mcm.resource_container_items;
SELECT distinct resource_container_type FROM mcm.resource_containers where owner_type = 'App\\Models\\Episode' ;
SELECT distinct resource_container_type FROM mcm.resource_containers where owner_type = 'App\\Models\\Programme' ;
SELECT distinct resource_container_type FROM mcm.resource_containers where owner_type = 'App\\Models\\Cast' ;

select 
    r.id as resource_container_id,
    r.owner_id as episode_id,
    r.owner_type as owner_type,
    r.resource_container_type as resource_container_type,
    r.resource_container_type as episode_catchup,
    ri.resource_container_item_type as resource_container_item_type,
    ri.resource_container_item_id as video_id,
    r.created_at as created_at,
    v.duration as duration
from  
	mcm.resource_containers r
inner join 
	mcm.resource_container_items ri
inner join
	mcm.videos v
where 
	r.owner_type = 'App\\Models\\Episode' and
    r.id = ri.resource_container_id and
    r.owner_id = 254061 and
    ri.resource_container_item_id = v.id
;

select 
    r.id as bonus_id,
    r.owner_id as programme_id,
    r.created_at as created_at,
    r.resource_container_type as bonus_type
from  
	mcm.resource_containers r
inner join 
	mcm.resource_container_items ri
inner join
	mcm.videos v
where 
	r.owner_type = 'App\\Models\\Programme' and
    r.id = ri.resource_container_id and
    /*r.owner_id = 254061 and*/
    ri.resource_container_item_id = v.id
    /*v.duration is NULL*/
;

/* checking video has episode_id  */
select 
    r.id as resource_container_id,
    0 as bonus_id,
    r.owner_id as episode_id,
    r.owner_type as owner_type,
    r.resource_container_type as resource_container_type,
    r.resource_container_type as episode_catchup,
    ri.resource_container_item_type as resource_container_item_type,
    ri.resource_container_item_id as video_id,
    r.created_at as created_at,
    v.duration as duration
from  
	mcm.resource_containers r
inner join 
	mcm.resource_container_items ri
inner join
	mcm.videos v
where 
	r.owner_type = 'App\\Models\\Episode' and
    r.id = ri.resource_container_id and
    /**r.owner_id = 254061 and**/
    /**v.id = 254060 and**/
    ri.resource_container_item_id = v.id
;

/* cheking video has bonus_id  */
select 
    r.id as bonus_id,
	0 as episode_id,
    r.owner_id as programme_id,
    r.owner_type as owner_type,
    r.resource_container_type as resource_container_type,
    r.resource_container_type as episode_catchup,
    ri.resource_container_item_type as resource_container_item_type,
    ri.resource_container_item_id as video_id,
    r.created_at as created_at,
    v.duration as duration
from  
	mcm.resource_containers r
inner join 
	mcm.resource_container_items ri
inner join
	mcm.videos v
where 
	r.owner_type = 'App\\Models\\Programme' and
    r.id = ri.resource_container_id and
    /**r.owner_id = 254061 and**/
    /**v.id = 254060 and**/
    ri.resource_container_item_id = v.id
;
- - - - - only for checking end — - - - - 
