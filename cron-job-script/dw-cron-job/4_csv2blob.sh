DATE=$(date +"%Y%m%d" -d"-2 days")
hadoop fs -ls -d wasb://dwexternal@tvbhkanalyticsstorage1.blob.core.windows.net/dwSnapshot/* | grep -q $DATE
if [ $? -ne 0 ]
then
	hadoop fs -mkdir wasb://dwexternal@tvbhkanalyticsstorage1.blob.core.windows.net/dwSnapshot/$DATE
	hadoop fs -mv wasb://dwexternal@tvbhkanalyticsstorage1.blob.core.windows.net/dwSnapshot/current/* wasb://dwexternal@tvbhkanalyticsstorage1.blob.core.windows.net/dwSnapshot/$DATE
else
	hadoop fs -rm wasb://dwexternal@tvbhkanalyticsstorage1.blob.core.windows.net/dwSnapshot/current/*	
fi
hadoop fs -copyFromLocal /home/adminssh/dwDailySnapshot/*.csv wasb://dwexternal@tvbhkanalyticsstorage1.blob.core.windows.net/dwSnapshot/current

DATE=$(date +"%Y%m%d")
echo `date` > /home/adminssh/dwDailySnapshot/log/"3+4_$DATE.log"
echo "Finished 3_csv2blob" >> /home/adminssh/dwDailySnapshot/log/"3+4_$DATE.log"
