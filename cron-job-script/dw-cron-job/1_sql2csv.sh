cd /home/angus.tsang/tvb-dataplatform/cron-job-script/dw-cron-job 
DATE=$(date +"%Y%m%d")

dbo_account_sql="SELECT boss_id, md5(mobile_number), customer_type, sales_channel, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at, status, id as account_id, title FROM boss_$DATE.accounts;"

dbo_artiste_info_sql="SELECT artiste_id, language, name, othername as other_name FROM mcm.artiste_infos;"

dbo_artiste_sql="SELECT id as artiste_id, age, if(gender = '' or gender is NULL, NULL, gender) as gender, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.artistes;"

dbo_attribute_list_sql="SELECT top_attribute_item_id as attribute_id,  attribute_list as child_attribute, title_tc, title_sc, title_en, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_attribute_list;"

dbo_attribute_programme_mapping_sql="SELECT programme_id, attribute_id, '1970-01-01 00:00:00' as created_at FROM sfm_mytvsuper.he_programme_attribute_relation;"

dbo_attribute_sql="SELECT attribute_id, title_tc, title_sc, title_en, status, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_attribute;"

dbo_bonus_sql="SELECT distinct r.id as bonus_id, r.owner_id as programme_id, if(r.created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', r.created_at), r.resource_container_type as bonsu_type FROM mcm.resource_containers r inner join mcm.resource_container_items ri on ri.resource_container_id = r.id where ri.resource_container_item_type = 'App\\\\Models\\\\Video' and r.owner_type = 'App\\\\Models\\\\Programme';" 

dbo_box_set_sql="SELECT id as box_set_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.box_sets;"

dbo_campaign_product_mapping_sql="SELECT product_id, campaign_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_$DATE.campaign_details;"

dbo_campaign_sql="SELECT id as campaign_id, code as campaign_code, description, campaign_type, period_type, if(effective_from is NULL, '1970-01-01 00:00:00', effective_from) as effective_from, effective_to, service_mean, channel_info, statement_desc_eng, statement_desc_chi, status, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_$DATE.campaigns;"

dbo_cast_artiste_mapping_sql="SELECT cast_id, artiste_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.artiste_cast;"

dbo_cast_info_sql="SELECT cast_id, language, name, othername as other_name, occupation, personality, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.cast_infos;"
#dbo_cast_info_sql="SELECT cast_id, language, name, othername as other_name, occupation, Null as personality, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.cast_infos;"

dbo_cast_sql="SELECT id as cast_id, box_set_id, gender, age, cast_type, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.casts;"

dbo_channel_sql="SELECT channel_no, network_code, start_time, end_time, name_tc, name_en, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_channel_info;"

dbo_crews_box_set_mapping_sql="SELECT box_set_id, crew_id, crew_type, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.box_set_crew;"

dbo_crews_info_sql="SELECT crew_id, language, name, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.crew_infos;"

dbo_crews_sql="SELECT id as crew_id, age, gender, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.crews;"

dbo_episode_cast_mapping_sql="SELECT episode_id, cast_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.cast_episode;"

dbo_episode_info_sql="SELECT episode_id, language, title, synopsis, synopsis_exad, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.episode_infos limit 1000;"
#dbo_episode_info_sql="SELECT episode_id, language, title, Null as synopsis, Null as synopsis_exad, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.episode_infos limit 1000;"

dbo_episode_sql="SELECT id as episode_id, box_set_id, episode_no, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM mcm.episodes;"

dbo_library_attribute_mapping_sql="SELECT lib_id as library_id, attribute_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_lib_top_attribute_item;"

dbo_library_programme_mapping_sql="SELECT programme_id, lib_id as library_id, '1970-01-01 00:00:00' as created_at FROM sfm_mytvsuper.he_lib_programme_relation;"

dbo_library_sql="SELECT lib_id as library_id, title_tc, title_sc, title_en, status, type, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_lib;"

dbo_product_sql="SELECT id as product_id, code, description, period_type, period, product_type, if(effective_from is NULL, '1970-01-01 00:00:00', effective_from) as effective_from, effective_to, statement_desc_eng, statement_desc_chi, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_$DATE.products;"

dbo_programme_box_set_mapping_sql="SELECT box_set_id, programme_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_box_sets;"

dbo_programme_sql="SELECT programme_id, programme_name, programme_name_en, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM sfm_mytvsuper.he_programmes;"
#dbo_programme_sql="SELECT id as programme_id, Null as programme_name_tc, Null as programme_name_en, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at from mcm.programmes;"
#dbo_programme_tc_info_sql="SELECT box_set_id as programme_id, title as programme_name_tc from mcm.box_set_infos where language = 'tc';"
#dbo_programme_en_info_sql="SELECT box_set_id as programme_id, title as programme_name_en from mcm.box_set_infos where language = 'en';"

dbo_subscription_product_mapping_sql="SELECT id as subscription_id, if(product_id is NULL, 1, product_id) as product_id, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_$DATE.subscribed_products;"

dbo_subscription_sql="SELECT account_id, id as subscription_id, if(campaign_id is NULL, 0, campaign_id) as campaign_id, contract_start, contract_end, if(created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', created_at) as created_at FROM boss_$DATE.subscriptions;"

dbo_ua_sql="SELECT ua, product_name, type, platform_type FROM nielsen_2016.maps_ua;"

dbo_video_bonus_sql="select ri.resource_container_item_id as video_id, r.id as bonus_id, 0 as episode_id, 0 as channel_no, if(v.duration > 0, v.duration, 0) as duration, if(v.created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', v.created_at) as created_at from mcm.resource_containers r inner join mcm.resource_container_items ri inner join mcm.videos v where r.owner_type = 'App\\\\Models\\\\Programme' and r.id = ri.resource_container_id and ri.resource_container_item_id = v.id;"

dbo_video_catchUp_sql="select ri.resource_container_item_id as video_id, 0 as bonus_id, r.owner_id as episode_id, 0 as channel_no, if(v.duration > 0, v.duration, 0) as duration, if(v.created_at = '0000-00-00 00:00:00', '1970-01-01 00:00:00', v.created_at) as created_at from mcm.resource_containers r inner join mcm.resource_container_items ri inner join mcm.videos v where r.owner_type = 'App\\\\Models\\\\Episode' and r.id = ri.resource_container_id and ri.resource_container_item_id = v.id;"

cd /home/angus.tsang/tvb-dataplatform/cron-job-script/dw-cron-job 
rm /home/angus.tsang/tvb-dataplatform/cron-job-script/dw-cron-job/*.csv

mysql -h db1.snap.boss.hk4.inline.tvb.com --user=boss --password=01a8e8dd1e0b22bbd418bbc340e4ed53 --default-character-set=utf8 -e "$dbo_account_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_account.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_artiste_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_artiste.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_artiste_info_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_artiste_info.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_attribute_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_attribute.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_attribute_list_sql" | sed "s/\"//g" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_attribute_list.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_attribute_programme_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_attribute_programme_mapping.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_bonus_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_bonus.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_box_set_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_box_set.csv

mysql -h db1.snap.boss.hk4.inline.tvb.com --user=boss --password=01a8e8dd1e0b22bbd418bbc340e4ed53 --default-character-set=utf8 -e "$dbo_campaign_sql" | sed 's/\\n//g' | sed 's/\r//g' | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_campaign.csv

mysql -h db1.snap.boss.hk4.inline.tvb.com --user=boss --password=01a8e8dd1e0b22bbd418bbc340e4ed53 --default-character-set=utf8 -e "$dbo_campaign_product_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_campaign_product_mapping.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_cast_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_cast.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_cast_artiste_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_cast_artiste_mapping.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_cast_info_sql" | sed 's/\\n//g' | sed 's/\r//g' | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_cast_info.csv
#mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_cast_info_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_cast_info.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_channel_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_channel.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_crews_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_crews.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_crews_box_set_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_crews_box_set_mapping.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_crews_info_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_crews_info.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_episode_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_episode.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_episode_cast_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_episode_cast_mapping.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_episode_info_sql" | sed "s/\"/\'/g" | sed 's/\\n//g' | sed 's/\r//g' | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_episode_info.csv
#mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_episode_info_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_episode_info.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_library_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_library.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_library_attribute_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_library_attribute_mapping.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_library_programme_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_library_programme_mapping.csv


mysql -h db1.snap.boss.hk4.inline.tvb.com --user=boss --password=01a8e8dd1e0b22bbd418bbc340e4ed53 --default-character-set=utf8 -e "$dbo_product_sql" | sed 's/\\n//g' | sed 's/\r//g' | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_product.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_programme_sql" | sed "s/\"/\'/g" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_programme.csv
#mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_programme_sql" | sed "s/\"/\'/g" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_programme.csv
#mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_programme_tc_info_sql" | sed "s/\"/\'/g" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_programme_tc_info.csv
#mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_programme_en_info_sql" | sed "s/\"/\'/g" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_programme_en_info.csv

mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_programme_box_set_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_programme_box_set_mapping.csv

mysql -h db1.snap.boss.hk4.inline.tvb.com --user=boss --password=01a8e8dd1e0b22bbd418bbc340e4ed53 --default-character-set=utf8 -e "$dbo_subscription_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_subscription.csv

mysql -h db1.snap.boss.hk4.inline.tvb.com --user=boss --password=01a8e8dd1e0b22bbd418bbc340e4ed53 --default-character-set=utf8 -e "$dbo_subscription_product_mapping_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_subscription_product_mapping.csv

mysql -h db2.nielsen.hk3.tvb.com --user=root --password=1@#$ --default-character-set=utf8 -e "$dbo_ua_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_ua.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_video_catchUp_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_video_catchUp.csv

mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_video_bonus_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_video_bonus.csv

echo `date` > /home/angus.tsang/dwDailySnapshot/log/"1+2_$DATE.log"
echo "Finished 1_sql2csv" >> /home/angus.tsang/dwDailySnapshot/log/"1+2_$DATE.log"

#mysql -h db1.snap.boss.hk4.inline.tvb.com --user=boss --password=01a8e8dd1e0b22bbd418bbc340e4ed53 --default-character-set=utf8 -e "$dbo_*_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_*.csv
#mysql -h db1.qa.hk3.tvb.com --user=mcm --password=z7FVp0tKehsb --default-character-set=utf8 -e "$dbo_*_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_*.csv
#mysql -h db1.stat.mytvsuper.hk4.inline.tvb.com --user=sfm_mytvsuper_st --password=FhPPy_spHd9@P4j% --default-character-set=utf8 -e "$dbo_*_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_*.csv
#mysql -h db2.nielsen.hk3.tvb.com --user=root --password=1@#$ --default-character-set=utf8 -e "$dbo_*_sql" | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > dbo_*.csv
