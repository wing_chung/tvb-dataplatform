use nielsendata;
add jar wasbs://libs@tvbhkanalyticsstorage1.blob.core.windows.net/csv-serde-1.1.2-0.11.0-all.jar;
insert into streamdata_optimized
select 
	s.visitor as visitor, 
	s.videoid as vid, 
	s.upid as upid,
	s.muid as muid,
	min(cast(from_unixtime(unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC'),'yyyy-MM-dd HH:mm:ss') as timestamp)) as start_at,
	max(cast(from_unixtime(unix_timestamp(concat(s.hkdate, ' ' , s.hktime), 'yyyy-MM-dd HH:mm:ss', 'UTC'),'yyyy-MM-dd HH:mm:ss') as timestamp)) as end_at,
	min(cast(s.timespent as int)) as duration_from,
	max(cast(s.timespent as int)) as duration_to,
	s.ua as ua,
	collect_list(s.countryname)[0],
	collect_list(s.cityname)[0], 
	collect_list(s.customer_stage)[0],
	collect_list(s.video_stage)[0], 
	collect_list(s.network_type)[0] 
from streamdata s 
where s.ua = '21' or s.ua = '22' or s.ua = '23' or s.ua = '24' 
group by visitor, videoid, upid, muid, ua;
