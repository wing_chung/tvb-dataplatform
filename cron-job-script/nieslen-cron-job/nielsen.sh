# 1. Download and extract ZIP
FTP_URL="ftp://SiteCensusUpload:D82prapr@ftp.tvb.com.hk/"
FTP_FOLDER="New_Tagging/"

if [ -z $1 ]; then
        if [ `uname -s` = "Darwin" ]; then
                DATE=$(date -v-1d +%Y%m%d)
        else
                DATE=$(date +"%Y%m%d" -d"-1 days")
        fi
else
        DATE=$1
fi
echo $DATE

ZIP=${DATE}.tar.gz

echo $FTP_URL$FTP_FOLDER$ZIP

wget -q $FTP_URL$FTP_FOLDER$ZIP
if [ $? -ne 0 ]
then
	echo "ERROR: FTP $ZIP does not exist"
	exit 1
else
        tar -zxvf $ZIP
        if [ $? -ne 0 ]; then
                echo "ERROR: tar failed"
		exit 1
        else
		hadoop fs -copyFromLocal ./Raw_Data_hktvb-streamdata_$DATE.csv wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata
		hadoop fs -copyFromLocal ./Raw_Data_hktvb-webdata_$DATE.csv wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/webdata
	fi
fi

# 2. CSV to ORC
hive -f csv2orc.sql

# 3. Rename the ORC files
hadoop fs -mv wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata_orc/000000_0 wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata_orc/$DATE
hadoop fs -mv wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/webdata_orc/000000_0 wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/webdata_orc/$DATE

# 4. CSV to OPTimized
hive -f csv2opt.sql
 
# 5. Remove the CSV files
hadoop fs -rm wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/streamdata/Raw_Data_hktvb-streamdata_$DATE.csv
hadoop fs -rm wasb://nielsendata@tvbhkanalyticsstorage1.blob.core.windows.net/webdata/Raw_Data_hktvb-webdata_$DATE.csv
